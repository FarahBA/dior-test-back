# Utilisation de l'image Node.js officielle en tant qu'image de base
FROM node:14

# Création d'un répertoire de travail dans l'image Docker
WORKDIR /app

# Copie du package.json et le package-lock.json (si disponible) dans le répertoire de travail
COPY package*.json ./

# Installation des dépendances du projet
RUN npm install

# Copie du reste des fichiers du projet dans le répertoire de travail
COPY . .

# Exposition du port sur lequel l'application Express écoute
EXPOSE 3001

# Démarrage de l'application lorsque le conteneur Docker est lancé
CMD ["npm", "start"]