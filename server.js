const express = require('express');
const cors = require('cors');
const app = express();
const port = 3001;

// Middleware CORS pour gérer les requêtes Cross-Origin
app.use(cors({
  origin: '*',
}));

// Mock de données pour les utilisateurs
const users = [
  { id: 1, username: 'farah', password: '123' },
  { id: 2, username: 'dior', password: 'dior' },
  { id: 3, username: 'utilisateur3', password: 'motdepasse3' }
];

// Mock de données pour les produits
const products = [
  { id: 0, image: 'product1.png', name: 'MEN SHIRT', price: '19000' },
  { id: 1, image: 'product2.png', name: 'LADY BAG', price: '3000' },
  { id: 2, image: 'product3.png', name: 'DIOR JEWERELY', price: '100 000' }
];

// Middleware pour permettre l'analyse des données JSON
app.use(express.json());

/**
 * Route POST pour l'authentification des utilisateurs.
 * @param {string} username - Nom d'utilisateur.
 * @param {string} password - Mot de passe de l'utilisateur.
 * @returns {JSON} - Réponse JSON avec un message et les données de l'utilisateur.
 */
app.post('/api/login', (req, res) => {
  const { username, password } = req.body.variables;
  // Vérifier si les informations d'identification sont valides
  const user = users.find((u) => u.username === username && u.password === password);
  if (!user) {
    return res.status(401).json({ message: 'Your login or password are incorrect' });
  }

  // Authentification réussie
  return res.json({ data: { message: 'Connexion réussie', user: { id: user.id, username: user.username } } });
});

/**
 * Route GET pour obtenir la liste des produits.
 * @returns {JSON} - Liste des produits au format JSON.
 */
app.get('/api/products', (req, res) => {
  res.json(products);
});

// Démarrage du serveur
app.listen(port, () => {
  console.log(`Serveur en cours d'exécution sur http://localhost:${port}`);
});